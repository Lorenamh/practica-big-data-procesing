package io.keepcoding.data.simulator.streaming

import java.sql.Timestamp
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

import org.apache.spark.sql.{DataFrame, SparkSession}

case class AntennaMessage(timestamp: Timestamp, id: String, antenna_id: String, bytes: Long, app:String)

trait StreamingProyectoJobJob {

  val spark: SparkSession

  def readFromKafka(kafkaServer: String, topic: String): DataFrame

  def parserJsonData(dataFrame: DataFrame): DataFrame

  def readAntennaMetadata(jdbcURI: String, jdbcTable: String, user: String, password: String): DataFrame

  //def enrichAntennaWithMetadata(antennaDF: DataFrame, metadataDF: DataFrame): DataFrame

  def computeBytesAgg(dataFrame: DataFrame): DataFrame
  def writeToConsole(dataFrame: DataFrame): Future[Unit]
  def writeToJdbc(dataFrame: DataFrame, jdbcURI: String, jdbcTable: String, user: String, password: String): Future[Unit]

  def writeToStorage(dataFrame: DataFrame, storageRootPath: String): Future[Unit]

  def run(args: Array[String]): Unit = {
    val Array(kafkaServer, topic, jdbcUri, jdbcMetadataTable, aggJdbcTable, jdbcUser, jdbcPassword, storagePath) = args
    println(s"Running with: ${args.toSeq}")

    val kafkaDF = readFromKafka(kafkaServer, topic)

    val antennaDF = parserJsonData(kafkaDF)

    val bytesDF = computeBytesAgg(antennaDF)
//      bytesDF
//        .writeStream
//      .format("console")
//      .start()
//      .awaitTermination()


    val storageFuture = writeToStorage(antennaDF, storagePath)

    val aggFuture = writeToJdbc(bytesDF, jdbcUri, aggJdbcTable, jdbcUser, jdbcPassword)

   // Await.result(Future.sequence(Seq(storageFuture)), Duration.Inf)
   //Await.result(Future.sequence(Seq(aggFuture)), Duration.Inf)
    Await.result(Future.sequence(Seq(aggFuture, storageFuture)), Duration.Inf)

    spark.close()
  }

}
