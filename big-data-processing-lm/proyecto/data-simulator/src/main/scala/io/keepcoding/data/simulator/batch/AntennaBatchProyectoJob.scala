package io.keepcoding.data.simulator.batch

import io.keepcoding.data.simulator.bach.BatchProyectoJob

import org.apache.spark.sql.functions._

import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}

import java.time.OffsetDateTime


object AntennaBatchProyectoJob extends BatchProyectoJob {



    override val spark: SparkSession = SparkSession
      .builder()
      .master("local[*]")
      .appName("Spark SQL KeepCoding Base")
      .getOrCreate()

    import spark.implicits._
    override def readFromStorage(storagePath: String, filterDate: OffsetDateTime): DataFrame = {
      spark
        .read
        .format("parquet")
        .load(s"${storagePath}/data")
        .filter(
          $"year" === filterDate.getYear &&
            $"month" === filterDate.getMonthValue &&
            $"day" === filterDate.getDayOfMonth &&
            $"hour" === filterDate.getHour
        )
    }

    override def readAntennaMetadata(jdbcURI: String, jdbcTable: String, user: String, password: String): DataFrame = {
      spark
        .read
        .format("jdbc")
        .option("url", jdbcURI)
        .option("dbtable", jdbcTable)
        .option("user", user)
        .option("password", password)
        .load()

    }


    override def computeBytesAgg(dataFrame: DataFrame): DataFrame = {

      dataFrame
        .select($"timestamp",$"bytes",expr("stack(3,'user_total_bytes',id, 'antenna_total_bytes',antenna_id,'app_total_bytes',app) as (type,id)"))
        ///  .select($"timestamp", $"id", $"bytes",$"app", $"antenna_id")
        .groupBy($"id",$"type", window($"timestamp", "1 hour"))
        .agg(sum($"bytes").as("value"),
        )
        .select($"window.start".as("timestamp"), $"id", $"value",$"type")


    }

    override def enrichAntennaWithMetadata(BytesAggDF: DataFrame, UserMetadataDF: DataFrame): DataFrame = {
      BytesAggDF
        .filter($"type" ===lit("user_total_bytes")).select($"timestamp",$"id",$"value".as("usage")) .as("AggUser")
        .join(
          UserMetadataDF.as("metadataUser"),
          $"AggUser.id" === $"metadataUser.id"
        ).drop($"AggUser.id")
        .drop($"name")
        .drop ($"metadataUser.id")


    }

    override def computeUserExceed(dataFrame: DataFrame): DataFrame = {
      dataFrame
        .filter($"usage">$"quota")
    }


    override def writeToJdbc(dataFrame: DataFrame, jdbcURI: String, jdbcTable: String, user: String, password: String): Unit = {
      dataFrame
        .write
        .mode(SaveMode.Append)
        .format("jdbc")
        .option("driver", "org.postgresql.Driver")
        .option("url", jdbcURI)
        .option("dbtable", jdbcTable)
        .option("user", user)
        .option("password", password)
        .save()
    }

    override def writeToStorage(dataFrame: DataFrame, storageRootPath: String): Unit = {
      dataFrame
        .write
        .partitionBy("year", "month", "day", "hour")
        .format("parquet")
        .mode(SaveMode.Overwrite)
        .save(s"${storageRootPath}/historical")
    }
  def main(args: Array[String]): Unit = run(args)
}
