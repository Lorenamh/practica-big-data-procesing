package io.keepcoding.data.simulator.streaming

import org.apache.spark.sql.catalyst.ScalaReflection
import org.apache.spark.sql.functions.{avg, col, dayofmonth, from_json, hour, lit, max, min, month, window, year}
import org.apache.spark.sql.streaming.OutputMode
import org.apache.spark.sql.types.{StringType, StructType, TimestampType}
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession, functions}
import org.apache.spark.sql.functions._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object AntennaStreamingProyectoJob extends StreamingProyectoJobJob {


  def main(args: Array[String]): Unit = run(args)

  override val spark: SparkSession = SparkSession
    .builder()
    .master("local[20]")
    .appName("Spark SQL KeepCoding Base")
    .getOrCreate()

  import spark.implicits._

  override def readFromKafka(kafkaServer: String, topic: String): DataFrame = {

    spark
      .readStream
      .format("kafka")
      .option("kafka.bootstrap.servers", kafkaServer)
      .option("subscribe", topic)
      .load()

  }

  override def parserJsonData(dataFrame: DataFrame): DataFrame = {
    val antennaMessageSchema: StructType = ScalaReflection.schemaFor[AntennaMessage].dataType.asInstanceOf[StructType]

    dataFrame
      .select(from_json(col("value").cast(StringType), antennaMessageSchema).as("json"))
      .select("json.*")
      .withColumn("timestamp", $"timestamp".cast(TimestampType))

  }

  override def readAntennaMetadata(jdbcURI: String, jdbcTable: String, user: String, password: String): DataFrame = {
    spark
      .read
      .format("jdbc")
      .option("url", jdbcURI)
      .option("dbtable", jdbcTable)
      .option("user", user)
      .option("password", password)
      .load()
  }

  //override def enrichAntennaWithMetadata(antennaDF: DataFrame, metadataDF: DataFrame): DataFrame = ???
    override def writeToConsole(dataFrame: DataFrame) : Future[Unit] = Future   {

    dataFrame
      .writeStream
        .format("console")
        .start()
        .awaitTermination()
    }
  override def computeBytesAgg(dataFrame: DataFrame): DataFrame = {

     dataFrame
       .select($"timestamp",$"bytes",expr("stack(3,'user_total_bytes',id, 'antenna_total_bytes',antenna_id,'app_total_bytes',app) as (type,id)"))
    ///  .select($"timestamp", $"id", $"bytes",$"app", $"antenna_id")
      .withWatermark("timestamp", "1 minute")
      .groupBy($"id",$"type", window($"timestamp", "5 minutes"))
      .agg(sum($"bytes").as("value"),
      )
      .select($"window.start".as("timestamp"), $"id", $"value",$"type")
/*    dataFrame
      .select($"timestamp", $"id", $"bytes")
      .withWatermark("timestamp", "30 seconds")
      .groupBy($"id", window($"timestamp", "1 minutes"))
      .agg(sum($"bytes").as("value"),
      )
      .withColumn("type",lit("user_total_bytes"))
      .select($"window.start".as("timestamp"), $"id", $"value",$"type")*/

  }

  override def writeToJdbc(dataFrame: DataFrame, jdbcURI: String, jdbcTable: String, user: String, password: String): Future[Unit] = Future {
    dataFrame
      .writeStream
      .foreachBatch { (data: DataFrame, batchId: Long) =>
        data
          .write
          .mode(SaveMode.Append)
          .format("jdbc")
          .option("driver", "org.postgresql.Driver")
          .option("url", jdbcURI)
          .option("dbtable", jdbcTable)
          .option("user", user)
          .option("password", password)
          .save()
      }
      .start()
      .awaitTermination()
  }

  override def writeToStorage(dataFrame: DataFrame, storageRootPath: String): Future[Unit] = Future {
    val columns = dataFrame.columns.map(col).toSeq ++
      Seq(
        year($"timestamp").as("year"),
        month($"timestamp").as("month"),
        dayofmonth($"timestamp").as("day"),
        hour($"timestamp").as("hour")
      )

    dataFrame
      .select(columns: _*)
      .writeStream
      .partitionBy("year", "month", "day", "hour")
      .format("parquet")
      .option("path", s"${storageRootPath}/data")
      .option("checkpointLocation", s"${storageRootPath}/checkpoint")
      .start()
      .awaitTermination()
  }


}
