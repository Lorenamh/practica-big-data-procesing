# Practica Big data procesing

**1. Creación instancia google compute engine**

- Creamos una instancia con e2-standar-4, permitiendo el tráfico http y https.
- En ficha instancia instalamos kafka (2.12.2.8.1)
- Crearemos además una regla de firewall que permita conectar cualquier sitio con el puerto 9092 ( etiqueta de red)
- Añadimos la regla de cortafuego ( la etiqueta) a la instancia
- Iniciamos zookeeper y kafka habiendo modificado previamente el fichero server.properties con la ip externa de la instancia


**2. Creación instancia Postgres**

**3. Creación tablas en Postgres**

Una vez en funcionamiento, debemos crear las tablas que alimentaremos más adelante y aquellas que contiene metadatos.
Para ello ejecutaremos el job de spark JdbcProvisioner

![alt text](img/dt.png "Title Text")

Comprobamos que la tabla user_metadata se ha cargado correctamente

![alt text](img/users.png "Title Text")

**4. Ejecución Docker**

En local ejecutamos el siguiente docker:

>docker run -it -e KAFKA_SERVERS=xxxxxxx:9092 andresgomezfrr/data-simulator:1.1

Modificando la ip externa de kafka 

![alt text](img/docker.png "Title Text")

**5. Creación del consumidor kafka**

En primer lugar creamos el topic y después el consumidor

> /root/kafka_2.12-2.8.1/bin/kafka-topics.sh --create --partitions 4 --replication-factor 1 --zookeeper localhost:2181 --topic devices

>/root/kafka_2.12-2.8.1/bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic devices

Comprobamos que los datos llegan a kafka correctamente

![alt text](img/Kafka.png "Title Text")


**6. Creación job spark ( lectura kafka, carga en postgres y en storage)**

- Creamos SparkSession
- Leemos de kafka
- Parseamos la información que llega de kafka, para eso le pasamos el esquema que tenemos definido en la case class AntennaMessage. Además casteamos la variable timestamp
- Hacemos la agregación. En este caso tenemos que pasar información de este formato:

|timestamp           |  id                                  | antenna_id                           |  app     | bytes  |
| ---                | ---                                  | ---                                  |---       | ---    |
|2020-09-20 12:12:35 | 00000000-0000-0000-0000-000000000012 | 11111111-1111-1111-1111-111111111111 | FACETIME | 3456   |

 a este otro:
 |timestamp           |  id                                  | value                 |  type              |
 | ---                | ---                                  | ---                   |---                 |
 |2020-09-20 14:00:00 |33333333-3333-3333-3333-333333333333  |454566                 |antenna_bytes_total |
 |2020-09-20 14:00:00 |FACEBOOK                              |45345                  |app_bytes_total |
 |2020-09-20 14:00:00 |00000000-0000-0000-0000-000000000012  |10405                  |user_bytes_total |
 
 Para ello usamos la siguiente sintaxis
 
```  dataFrame
      .select($"timestamp",$"bytes",expr("stack(3,'user_total_bytes',id, 'antenna_total_bytes',antenna_id,'app_total_bytes',app) as (type,id)"))
      .withWatermark("timestamp", "1 minute")
      .groupBy($"id",$"type", window($"timestamp", "5 minutes"))
      .agg(sum($"bytes").as("value"),
      )
      .select($"window.start".as("timestamp"), $"id", $"value",$"type") 
      
```
  El fragmento anterior por un lado lo que hace es crear como variable los nombres de las columnas :
  
   |id   | A      | B     | C     |
   |---  | ---    | ---   | ---   |
   |1    | 20     | rojo  | 00-00 |
   |2    | 25     | azul  | 00-01 |
   
   con la función stack:
   
``` df.select($"id",expr("stack(3,'A',A,'B',B,'C',C) as (type,value)"))
```
   
   |id   | type      | value    |
   |---  | ---       | ---      |
   |1    | A         | 20       |
   |1    | B         | rojo     |   
   |1    | C         | 00-00    |      
   |2    | A         | 205      |
   |2    | B         | azul     |   
   |2    | C         | 00-01    |   
      
 Además creamos una ventana de tiempo de 1 minuto para posteriormente hacer métricas agregadas cada 5 minuto, agregando por id y type. Para obtener la suma total de bytes por usuario, aplicación y antenna
 
 - Cargamos la información en Postgres
 select * from bytes; 
 
 ![alt text](img/bytes.png "Title Text")
 
 - Guardamos también la información en storage
 
 
  ![alt text](img/Parquet.png "Title Text")
 
**7. Analítica de clientes**

Creación job de Spark para la parte Batch.
Obtenemos la suma por usuario, aplicación y antena en una hora que pasamos como parámetro:


  ![alt text](img/bytes_hourly.png "Title Text")


Y por último comprobamos aquellos clientes que se han pasado en una hora el límite, para ello simplemente filtramos la tabla bytes_hourly por type, cuando es user_bytes_total e incluimos un filtro para aquellos clientes donde usage es mayor a quota

```BytesAggDF
        .filter($"type" ===lit("user_total_bytes")).select($"timestamp",$"id",$"value".as("usage")) .as("AggUser")
        .join(
          UserMetadataDF.as("metadataUser"),
          $"AggUser.id" === $"metadataUser.id"
        ).drop($"AggUser.id")
        .drop($"name")
        .drop ($"metadataUser.id") 
        
```

```dataFrame
        .filter($"usage">$"quota") 
```

  ![alt text](img/limituser.png "Title Text")







